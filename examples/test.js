const fetch = require('node-fetch');
const colors = require('colors');
const nlp = require('nlp_compromise');
const giAPI = require('../lib/giAPI');
const giUtility = require('../lib/giUtility');


const expressions = [
    'I earn 25K a year', `25000`,

    `I make 78,897`, `78897`,

    `120234`, '120234',

    `23,,234`, ``,

    `23k234`, ``,

    `23234  23000`, `23234`,

    `my 2nd income is 23000`, `23000`,
    
    `around 20k-ish`, ``,

    `4,5,4`, ``,

    `qewrqwer`, ``,

    `I dont know yet`, ``
];


(() => {
    for (let i = 0; i < expressions.length / 2; i += 2) {
        let text = expressions[i].toLowerCase();

        let num;
        try {
            // num = text.match(/(\s|^)(\d+)(,?)(\d+)([k]?)(\s|$)/)[0];
            num = text.match(/(\s|^)(\d+)(,?)(\d+)(k?)(\s|$)/)[0].replace(/,/, "").replace(/k/,"000").trim();
        }catch(err){
            num = "";
        }


        if(num !== expressions[i+1]) {
            console.log(`Fail at: ${expressions[i]} => ${num}`);
        }
    }
})()