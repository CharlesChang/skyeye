const nlp = require('nlp_compromise');
const fetch = require('node-fetch');
let haveError = 0;
let subjects_age = [
    `30`,`30`,
    `my 2nd kid is thirty years old`, `30`,
    `my 2nd kid is 30 years old`,`30`,
    `ten`, `10`,
    `im thirty four years old`, `34`,
    `i'm in my 30s`, `0`,
    `my 3rd son is in my 30s`, `0`,
    `my 10th son is 30`, `30`,
    `my 1st son is 30`, `30`,
]

function standardize(text) {
    text = nlp.text(text.toLowerCase());
    text = text.contractions.expand();
    text = text.normal();

    return text;
}

function extract_age(text) {
    let num;
    try {
        num = text.match(/\d+$/)[0].toString();
    } catch (err) {
        num = nlp.value(text).number.toString();
    }
    return num
}

function startTest(){
    for(let i = 0; i< subjects_age.length; i+=2){
        let text = standardize(subjects_age[i])
        let extraction = extract_age(text);
        if( extraction !== subjects_age[i+1]){
            console.log('\x1b[31m', `age extraction failure: ${i/2+1}`, '\x1b[0m');
            console.log(`
                receive: ${subjects_age[i]}
                expect: ${subjects_age[i+1]}
                but get: ${extraction}
            `);
            haveError++;
        }
    }

    if(haveError === 0){
        console.log('\x1b[32m', `No Error Found`, '\x1b[0m');
    }else{
        console.log('\x1b[32m', `Total number of error: ${haveError}`, '\x1b[0m');
    }
}

startTest();

