var fs = require('fs');

class Logger {
    constructor() {
        this.log_askingZipcode = fs.createWriteStream('./logs/askingZipcode.txt', {
            flags: 'a' // 'a' means appending (old data will be preserved)
        });

        this.log_askingAge = fs.createWriteStream('./logs/askingAge.txt', {
            flags: 'a' // 'a' means appending (old data will be preserved)
        });
        this.log_askingTobacco = fs.createWriteStream('./logs/askingTobacco.txt', {
            flags: 'a' // 'a' means appending (old data will be preserved)
        });
    }

    recordConvo(text, filename) {
        switch (filename) {
            case `askingZipcode`:
                this.log_askingZipcode.write(text);
                break;
            case `askingAge`:
                this.log_askingAge.write(text);
                break;
            case `askingTobacco`:
                this.log_askingTobacco.write(text);
                break;
        }
    }
}

module.exports = new Logger();