const fetch = require('node-fetch');
const giUtility = require('../lib/giUtility');
const config = require('./config.json');

const FB_PAGE_TOKEN = config.FB_PAGE_TOKEN;
if (!FB_PAGE_TOKEN) {
    throw new Error('missing FB_PAGE_TOKEN, in fbAPI.js')
}

class FbAPI {
    greetingText() {
        const body = JSON.stringify({
            "setting_type": "greeting",
            "greeting": {
                "text": "Hi {{user_first_name}}, welcome to this bot."
            }
        })

        return fetch(`https://graph.facebook.com/v2.6/me/thread_settings?access_token=${FB_PAGE_TOKEN}`, {
            method: "POST",
            headers: {
                'Content-Type': "application/json",
            },
            body
        }).then(rsp => {
            if (rsp.ok) {
                console.log(`greeting text successful initialized`);

            } else {
                console.log(`greeting text error:`, rsp.statusText);
            }
        }).catch(err => {
            console.log(`greeting text error:`, err)
        })
    }

    delete_greetingText() {
        const body = {
            "setting_type": "greeting"
        }

        return fetch(`https://graph.facebook.com/v2.6/me/thread_settings?access_token=${FB_PAGE_TOKEN}`, {
            method: `DELETE`,
            headers: {
                'Content-Type': `application/json`,
            },
            body
        }).then(rsp => {
            if (rsp.ok) {
                console.log(`delete greeting text successful initialized`);
            } else {
                console.log(`delete greeting text error:`, rsp.statusText)
            }

        }).catch(err => {

        })
    }
    getStarted() {
        const body = JSON.stringify({
            "setting_type": "call_to_actions",
            "thread_state": "new_thread",
            "call_to_actions": [
                {
                    "payload": `botSays=welcome to gigene`
                }
            ]
        });
        return fetch(`https://graph.facebook.com/v2.6/me/thread_settings?access_token=${FB_PAGE_TOKEN}`, {
            method: `POST`,
            headers: {
                "Content-Type": "application/json",
            },
            body
        }).then(rsp => {
            if (rsp.ok) {
                console.log(`fb getStarted Button successfully inilialized`);
            } else {
                console.log(`fb get started button error:`);
                console.log(rsp.body)
            }
        }).catch(err => console.log(`fb get started button error:`, err))


    }

    delete_getStarted() {
        const body = {
            "setting_type": "call_to_actions",
            "thread_state": "new_thread"
        }
        return fetch(`https://graph.facebook.com/v2.6/me/thread_settings?access_token=${FB_PAGE_TOKEN}`, {
            method: `DELETE`,
            headers: {
                'Content-Type': `application/json`,
            },
            body
        }).then(rsp => {
            rsp.json()
        }).then(json => {
            if (json.error && json.error.message) {
                throw new Error(json.error.message);
            } else {
            }
        }).catch(err => {
            console.log(`delete getstarted button error:`, err)
        })
    }

    async getFirstName(id) {
        return fetch(`https://graph.facebook.com/v2.6/${id}?access_token=${FB_PAGE_TOKEN}`, {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json'
            },
        }).then(rsp => {
            return new Promise((resolve, reject) => {
                if (rsp.ok) {
                    let json = rsp.json().then(json => {
                        resolve(json.first_name);
                    })
                } else {
                    reject(rsp.statusText);
                }
            })
        })
    }


    async enablePersistentMenu() {
        const body = JSON.stringify({
            "setting_type": "call_to_actions",
            "thread_state": "existing_thread",
            "call_to_actions": [{
                "type": "postback",
                "title": "Restart Conversation",
                "payload": `passAsTopic=serviceMenu`
            }, {
                "type": "postback",
                "title": "Ask a Question",
                "payload": `passAsTopic=askQuestion`
            }, {
                "type": "postback",
                "title": "Modify existing Infomation",
                "payload": giUtility.giPhone,
            }, {
                "type": "web_url",
                "title": "Visit GetInsured.com",
                "url": "https://www.getinsured.com",
                "webview_height_ratio": "full",
                "messenger_extensions": true
            }]
        });
        return fetch(`https://graph.facebook.com/v2.6/me/thread_settings?access_token=${FB_PAGE_TOKEN}`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: body
        }).then(rsp => {
            return new Promise((resolve, reject) => {
                let json = rsp.json().then(json => {
                    console.log(json);
                    resolve();
                })
            })
        }).catch(err => console.log(`persistent menu error:`, err.message))
    }


    whiteListing() {
        let domains = [
            "https://www.getinsured.com/",
            "https://51f76e274a2f4f1bc0de-118c5c8b2511ce3863dbabd7b37fba18.ssl.cf1.rackcdn.com",
            "https://public.hcsc.net",
            "https://www.anthem.com",
            "https://phix8qa.ghixqa.com",
            "https://repealandreplacecalculator.com",
            "https://35d5c6c0d371f8379485-677802d3fd2c5e35eddab79a9da6f529.ssl.cf2.rackcdn.com",
            "https://9b5ea118f3d68744fa04-04b308c70d701305ff31abd2a6fa528a.ssl.cf1.rackcdn.com/resources/issuers/logo//2015-10-1415:26:274/4148_40513Kaiser_1435254920613.png",
            "https://www.kaiserpermanente.org",
        ]
        let body = JSON.stringify({
            "setting_type": "domain_whitelisting",
            "whitelisted_domains": domains,
            "domain_action_type": "add"
        })
        fetch(`https://graph.facebook.com/v2.6/me/thread_settings?access_token=${FB_PAGE_TOKEN}`, {
            method: "POST",
            headers: {
                'Content-Type': 'application/json'
            },
            body
        })
            .then(rsp => rsp.json())
            .then(json => {
                if (json.error && json.error.message) {
                    throw new Error(json.error.message);
                } else {
                    console.log(`whitelisting domain works`)
                }
            });
    }

    typeing(USER_ID, on_off) {
        let onoff;
        if(on_off){
            onoff = "typing_on"
        }else{
            onoff = "typing_off"
        }
        const body = JSON.stringify({
            "recipient": {
                "id": USER_ID
            },
            "sender_action": onoff
        });

        fetch(`https://graph.facebook.com/v2.6/me/messages?access_token=${FB_PAGE_TOKEN}`, {
            method: "POST",
            headers: {
                'Content-Type': 'application/json'
            },
            body
        })
        .then( rsp =>{
             return new Promise((resolve, reject) => {
                if (rsp.ok) {
                    let json = rsp.json().then(json => {
                        resolve(json);
                    })
                } else {
                    reject(rsp.statusText);
                }
            })
        }).catch(
            err => console.log(`typing_on error:`, err.message)
        )
    }
}

module.exports = new FbAPI();