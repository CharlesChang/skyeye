const fetch = require('node-fetch');
class GiAPI {

    async validateZip(zip) {
        return new Promise((resolve, reject) => {
            if (zip.toString().length !== 5) {
                reject({
                    message: `There is a problem, Zipcode must be 5 digit.`
                });
            }

            // resolve to another promize, fetch
            const encoded = encodeURI(zip);
            fetch(`https://phix8qa.ghixqa.com/screener/api/validateZip?zipcode=` + encoded, {
                method: "GET",
                headers: {
                    'Content-Type': 'application/json'
                },

            }).then(rsp => {
                let json = rsp.json().then(json => {
                    if (Object.keys(json).length === 0) {
                        reject({
                            message: `This zipcode does not exist. Please double check.`
                        });
                    } else {
                        resolve(json);
                    }
                });
            }).catch(err => err)
        })
    }

    async screening(context) {
        let me = this;
        try {
            let info = {
                eventId: "10",
                zipCode: context.zipcode,
                countyCode: context.countyCode,
                stateCode: context.stateCode,
                familySize: parseInt(context.familySize),
                householdIncome: context.income,
                members: (() => {
                    let array = [];
                    for (let key in context.members) {
                        if (!context.members.hasOwnProperty(key)) {
                            // if we hit the object prototype, we skip
                            continue;
                        }
                        let el = {
                            isTobaccoUser: context.members[key].isTobaccoUser,
                            isSeekingCoverage: `Y`,
                            gender: "",
                            relationshipToPrimary: (() => {
                                if (key.includes(`child`)) {
                                    return `CHILD`
                                } else {
                                    return key.toUpperCase();
                                }
                            })(),
                            dateOfBirth: (() => {
                                return `10/10/${new Date().getFullYear() - parseInt(context.members[key].age)}`
                            })()
                        }

                        array.push(el);
                    }

                    return array;
                })(),
            }
            console.log(`payload for eligibility:`, info);


            let returnFromeligibility = await me.eligibility(info);
            console.log(`returnFromeligibility:`, returnFromeligibility);


            info.coverageStartDate = "11/1/2016";
            info.insuranceType = "HEALTH";
            info.exchangeType = "ON";
            info.minimizePlanData = true;
            info.showCatastrophicPlan = false;
            info.tenantCode = "GINS";
            info.maxSubsidy = (() => {
                // get the number out of it
                return parseFloat(returnFromeligibility.aptc.substring(1))
            })();
            info.costSharing = returnFromeligibility.csrLevel;
            info.pdMtCartItemMemberList = (() => {
                let array = [];
                for (let i = 0; i < returnFromeligibility.members.length; i++) {
                    let temp = returnFromeligibility.members[i];
                    let el = {
                        relationship: temp.relationshipToPrimary,
                        gender: "FEMALE",
                        dob: temp.dateOfBirth,
                        tobacco: temp.isTobaccoUser === `N` ? `NO` : `YES`
                    }
                    array.push(el);
                }
                return array;
            })();
            delete info.members


            let giPlanss = await me.giPlans(info);
            return giPlanss;
        } catch (err) {
            throw new Error(err);
        }

    }


    async eligibility(json) {
        const body = JSON.stringify(json)

        return fetch(`https://www.getinsured.com/screener/api/eligibility`, {
            method: "POST",
            headers: {
                'Content-Type': 'application/json'
            },
            body: body
        }).then(rsp => {
            return new Promise((resolve, reject) => {
                if (rsp.ok) {
                    let json = rsp.json().then(json => {
                        if (json.aptc === "N/A") {
                            reject(`eligibility => You are not eligible for any plan`);
                        } else if (json.csrLevel === "N/A") {
                            json.csrLevel = "CS1"
                            resolve(json);
                        } else {
                            resolve(json)
                        }
                    })
                }
            })
        })
    }

    async giPlans(json) {
        const body = JSON.stringify(json)
        return fetch(`https://www.getinsured.com/ghix-plandisplay/api/getRecommendedPlans`, {
            method: "POST",
            headers: {
                'Content-Type': 'application/json'
            },
            body: body
        }).then(rsp => {
            return new Promise((resolve, reject) => {
                if (rsp.ok) {
                    resolve(rsp.json().then(json => json))
                } else {
                    reject(`giPlans error => ${rsp.statusText}`);
                }
            })
        })
    }

}

module.exports = new GiAPI();