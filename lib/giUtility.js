class GiUtility {
    constructor() {
        this.giPhone = "+18666028466";
    }
    textTransform(id, text, data) {
        switch (text) {
            case "gi_yesnotobacco":
                const reply = (() => {
                    if (data === 'self') {
                        return `Did you use tobacco in the past 5 years?`
                    } else if (data === `spouse`) {
                        return `Did your spouse use tobacco in the past 5 years?`
                    } else {
                        return `Did ${data} use tobacco in the past 5 years?`
                    }
                })();
                return JSON.stringify({
                    recipient: {
                        id
                    },
                    message: {
                        text: reply,
                        quick_replies: [{
                            content_type: "text",
                            title: "Yes",
                            payload: `passAsText=yes`

                        }, {
                            content_type: "text",
                            title: "No",
                            payload: `passAsText=no`
                        }]
                    }
                });

            case "gi_getStarted":
                return JSON.stringify({
                    recipient: {
                        id
                    },
                    message: {
                        text: "Thank you for your interest. We need some information from you to determine the eligibility",
                        quick_replies: [{
                            content_type: "text",
                            title: "Yes. Let's proceed.",
                            payload: `passAsText=Yes. Let's proceed.`

                        }, {
                            content_type: "text",
                            title: "No. Maybe another time",
                            payload: `passAsText=No. Maybe another time{`
                        }]
                    }
                }, null, 4);

            case "gi_serviceMenu":
                return JSON.stringify({
                    recipient: {
                        id
                    },
                    message: {
                        attachment: {
                            type: "template",
                            payload: {
                                template_type: "generic",
                                elements: [
                                    {
                                        image_url: "https://phix3dev.ghixqa.com/resources/images/GetInsured_byVimo_1by2.png",
                                        title: "Welcome to Gigene",
                                        subtitle: "What can I do for you?",
                                        buttons: [
                                            {
                                                "type": "web_url",
                                                "url": "https://repealandreplacecalculator.com/screener/repealreplace.html#/",
                                                "title": "Obama vs Trump",
                                                "webview_height_ratio": "compact"
                                            },
                                            {
                                                type: "postback",
                                                title: "Buy Health Insurance",
                                                payload: `Buy Health Insurance`
                                            }, {
                                                type: "phone_number",
                                                title: "Call 855 531 4368",
                                                payload: "+18555314368"
                                            }
                                        ]
                                    }

                                ],


                            }
                        }
                    }
                }, null, 4);

            case "gi_longTemp":
                return this.generateLongTemp(id, data);
            case "gi_restartTemp":
                return JSON.stringify({
                    recipient: {
                        id
                    },
                    message: {
                        attachment: {
                            type: "template",
                            payload: {
                                template_type: "button",
                                text: "What can I do for you now?",
                                buttons: [{
                                    type: "postback",
                                    title: "Restart",
                                    payload: `passAsText=Restart`
                                }, {
                                    type: "phone_number",
                                    title: "Call Our Real Agent",
                                    payload: "+16505551234"
                                }]
                            }
                        }
                    }
                })
            case `gi_askingIfSpouse`:
                return JSON.stringify({
                    recipient: {
                        id
                    },
                    message: {
                        "attachment": {
                            "type": "template",
                            "payload": {
                                "template_type": "button",
                                "text": "Are you seeking coverage for your spouse this time?",
                                "buttons": [{
                                    "type": "postback",
                                    "title": "Yes, include spouse",
                                    "payload": `passAsText=yes`
                                }, {
                                    "type": "postback",
                                    "title": "No. I'm single.",
                                    "payload": `passAsText=No I'm single.`
                                }, {
                                    "type": "postback",
                                    "title": "No, exclude spouse.",
                                    "payload": `passAsText=No, exclude spouse`
                                }]
                            }
                        }
                    }
                });
            case `gi_askQuestions`:
                return JSON.stringify({
                    recipient: {
                        id
                    },
                    message: {
                        "attachment": {
                            "type": "template",
                            "payload": {
                                "template_type": "button",
                                "text": "How may I help you?",
                                "buttons": [{
                                    "type": "postback",
                                    "title": "What is copay?",
                                    "payload": `passAsText=What is copay`
                                }, {
                                    "type": "postback",
                                    "title": "What is premium",
                                    "payload": `passAsText=What is premium`
                                }]
                            }
                        }
                    }
                });

            case `gi_returnToFlow`:
                return JSON.stringify({
                    recipient: {
                        id
                    },
                    message: {
                        text: `Continue where we left off?`,
                        quick_replies: [{
                            content_type: "text",
                            title: "Continue",
                            payload: `passAsText=Yes, let's continue`

                        }, {
                            content_type: "text",
                            title: "Ask another",
                            payload: `passAsText=No, ask another question.`
                        }]
                    }
                });


            case `gi_askingIfChildren`:
                return JSON.stringify({
                    recipient: {
                        id
                    },
                    message: {
                        text: `Are you seeking coverage for your children this time?`,
                        quick_replies: [{
                            content_type: "text",
                            title: "Yes",
                            payload: `passAsText=yes`

                        }, {
                            content_type: "text",
                            title: "No",
                            payload: `passAsText=no`
                        }]
                    }
                });

            case `gi_callUs`:
                return JSON.stringify({
                    recipient: {
                        id
                    },
                    message: {
                        "attachment": {
                            "type": "template",
                            "payload": {
                                "template_type": "button",
                                "text": data.displayText,
                                "buttons": [{
                                    "type": "phone_number",
                                    "title": "Call Representative",
                                    "payload": this.giPhone
                                }]
                            }
                        }
                    }
                });

            case `gi_multipleCounty`:
                return JSON.stringify({
                    recipient: {
                        id
                    },
                    message: {
                        "text": "We found more than one county in your zipcode. Please select your county.",
                        "quick_replies": (() => {
                            let array = [];
                            for (let i = 0; i < data.county.length; i++) {
                                let el = {
                                    "content_type": "text",
                                    "title": data.county[i],
                                    "payload": `passAsText=${data.county[i]}`
                                }
                                array.push(el);
                            }
                            return array;
                        })(),
                    }
                })

            default:

                return JSON.stringify({
                    recipient: {
                        id
                    },
                    message: {
                        text
                    }
                }, null, 4);
        }
    }

    generateLongTemp(id, plans) {
        console.log( typeof plans)
        if (typeof plans === `object`) {
            let elements = (() => {
                let array = [];
                plans.forEach((plan) => {
                    let element = {
                        "title": plan.name,
                        "image_url": plan.issuerLogo,
                        "subtitle": ((plan) => `$${plan.premium}`)(plan),
                        "default_action": {
                            "type": "web_url",
                            "url": plan.providerLink,
                            "messenger_extensions": true,
                            "webview_height_ratio": "tall",
                            "fallback_url": "https://www.getinsured.com/"
                        },
                        "buttons": [{
                            "title": "View Details",
                            "type": "web_url",
                            "url": plan.providerLink,
                            "messenger_extensions": true,
                            "webview_height_ratio": "tall",
                            "fallback_url": "https://www.getinsured.com/"
                        }]
                    }

                    array.push(element);
                })
                return array
            })();
            const data = {
                recipient: {
                    id
                },
                message: {
                    "attachment": {
                        "type": "template",
                        "payload": {
                            "template_type": "list",
                            "elements": elements,
                            "buttons": [{
                                "title": "View More",
                                "type": "postback",
                                "payload": "#"
                            }]
                        }
                    }
                }
            }
            return JSON.stringify(data, null, 4)
        } else {
            console.log(`fail to generateLongTemp`)
            return JSON.stringify({
                recipient: {
                    id
                },
                message: {
                    attachment: {
                        type: "template",
                        payload: {
                            template_type: "button",
                            text: "I'm not able to fetch any plan.",
                            buttons: [{
                                type: "postback",
                                title: "Talk to the agents",
                                payload: "#"
                            }]
                        }
                    }
                }
            })
        }
    }

    detectPositive(text) {
        if (!!text.includes(`yes`) ||
            !!text.includes(`ok`) ||
            !!text.includes(`go ahead`) ||
            !!text.includes(`sure`)
        ) {
            return true;
        } else {
            return false;
        }
    }

    breakdownPayload(payload) {
        let obj = {};
        let array = payload.split(`&`);
        array.forEach(para => {
            let frag = para.split(`=`);
            obj[frag[0]] = frag[1];
        })
        return obj;
    }


}

module.exports = new GiUtility();