'use strict';

const bodyParser = require('body-parser');
const crypto = require('crypto');
const express = require('express');
const fetch = require('node-fetch');
const request = require('request');
const nlp = require('nlp_compromise');
const fs = require('fs');
const giAPI = require('./lib/giAPI');
const fbAPI = require('./lib/fbAPI');
const giUtility = require('./lib/giUtility');
const Logger = require('./lib/logger');
const config = require('./lib/config.json');

// Webserver parameter
const PORT = process.env.PORT || 4000;


// Messenger API parameters
// const FB_PAGE_TOKEN = process.env.FB_PAGE_TOKEN;
const FB_PAGE_TOKEN = config.FB_PAGE_TOKEN;
if (!FB_PAGE_TOKEN) {
    throw new Error('missing FB_PAGE_TOKEN')
}
// const FB_APP_SECRET = process.env.FB_APP_SECRET;
const FB_APP_SECRET = config.FB_APP_SECRET;
if (!FB_APP_SECRET) {
    throw new Error('missing FB_APP_SECRET')
}

const FB_VERIFY_TOKEN = config.FB_VERIFY_TOKEN;


// ----------------------------------------------------------------------------
// Messenger API specific code

// See the Send API reference
// https://developers.facebook.com/docs/messenger-platform/send-api-reference

const messageQ = {}
const fbMessage = (id, obj) => {
    // every message that comes in will be queued in messageQ[id]
    //  typeof [] === 'object'
    if (typeof messageQ[id] !== `object`) {
        messageQ[id] = [];
        messageQ[id].push(obj);
        syncFbMessage(id);
    } else {
        messageQ[id].push(obj);
    }
};

function syncFbMessage(id) {
    if (messageQ[id].length === 0) {
        // when finish executing, we delete the id to save memory
        delete messageQ[id];
        return;
    }
    const body = giUtility.textTransform(id, messageQ[id][0].text, messageQ[id][0].data);
    const qs = 'access_token=' + encodeURIComponent(FB_PAGE_TOKEN);
    return fetch('https://graph.facebook.com/me/messages?' + qs, {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body,
    })
        .then(rsp => rsp.json())
        .then(json => {
            if (json.error && json.error.message) {
                throw new Error(json.error.message);
            }

            // remove the first one, then execute again.
            messageQ[id].splice(0, 1);
            syncFbMessage(id);
            // return json;
        });
}

// ----------------------------------------------------------------------------
// Wit.ai bot specific code

// This will contain all user sessions.
// Each session has an entry:
// sessionId -> {fbid: facebookUserId, context: sessionState}
const sessions = {};



// Starting our webserver and putting it all together
const app = express();
app.use(({
    method,
    url
}, rsp, next) => {
    rsp.on('finish', () => {
        console.log(`${rsp.statusCode} ${method} ${url}`);
    });
    next();
});
app.use(bodyParser.json({
    verify: verifyRequestSignature
}));

// render some static files to the view
app.use(express.static(__dirname + '/public'));
app.set('views', __dirname + '/views');
app.set('view engine', 'html');
app.get('/', function (request, response) {
    response.sendFile(__dirname + '/views/index.html');
});
app.get('/iframe', function (request, response) {
    response.sendFile(__dirname + '/views/iframe.html');
});

// Webhook setup
app.get('/webhook', (req, res) => {
    if (req.query['hub.mode'] === 'subscribe' &&
        req.query['hub.verify_token'] === FB_VERIFY_TOKEN) {
        res.send(req.query['hub.challenge']);
        console.log(`webhook validation successful`)
    } else {
        res.sendStatus(400);
    }
});



// Message handler
app.post('/webhook', (req, res) => {
    // Parse the Messenger payload
    // See the Webhook reference
    // https://developers.facebook.com/docs/messenger-platform/webhook-reference
    const data = req.body;


    if (data.object === 'page') {
        data.entry.forEach(entry => {
            entry.messaging.forEach(event => {
                console.log(`event`, event);
                try {
                    let content = giUtility.breakdownPayload(event.postback.payload);
                    if (content.passAsText) {
                        event.message = {
                            text: content.passAsText
                        };
                    } else if (content.passAsTopic) {
                        const senderId = event.sender.id;
                        const sessionId = findOrCreateSession(senderId);
                        let context = sessions[sessionId].context;

                        context[`cachTopic`] = context.topic;
                        context[`topic`] = content.passAsTopic;
                        assignNestedObject(sessions, [sessionId, `context`], context);
                        bot(sessionId, null, senderId);
                    } else if (content.botSays) {
                        const senderId = event.sender.id;
                        fbMessage(senderId, {
                            text: `gi_serviceMenu`
                        });
                    }
                } catch (err) {
                    //do nothing
                    // console.warn(err);
                }

                if (event.message && !event.message.is_echo) {
                    // Yay! We got a new message!
                    // We retrieve the Facebook user ID of the sender
                    const senderId = event.sender.id;

                    // We retrieve the user's current session, or create one if it doesn't exist
                    // This is needed for our bot to figure out the conversation history
                    const sessionId = findOrCreateSession(senderId);

                    // We retrieve the message content
                    const {
                        text,
                        attachments,
                    } = event.message;

                    if (attachments) {
                        // We received an attachment
                        // Let's reply with an automatic message
                        fbMessage(senderId, {
                            text: 'Sorry I can only process text messages for now.'
                        })
                            .catch(console.error);
                    } else if (text) {
                        console.log(`text`, text);
                        // We received a text message

                        let t = nlp.text(text.toLowerCase());
                        t = t.contractions.expand();
                        t = t.normal();

                        bot(sessionId, t, senderId);
                    }
                } else {

                    // console.log('received event', JSON.stringify(event));
                    // console.log('received event');
                }
            });
        });
    }
    res.sendStatus(200);
});


const findOrCreateSession = (fbid) => {
    if (!fbid) {
        throw new Error(`Facebook id not found. Cannot create session.`);
    }
    let sessionId;
    // Let's see if we already have a session for the user fbid
    Object.keys(sessions).forEach(k => {
        if (sessions[k].fbid === fbid) {
            // Yep, got it!
            sessionId = k;
        }
    });
    if (!sessionId) {
        // No session found for user fbid, let's create a new one
        sessionId = new Date().toISOString();
        sessions[sessionId] = {
            fbid: fbid,
            context: {}
        };
    }
    return sessionId;
}
/*
 * Verify that the callback came from Facebook. Using the App Secret from
 * the App Dashboard, we can verify the signature that is sent with each
 * callback in the x-hub-signature field, located in the header.
 *
 * https://developers.facebook.com/docs/graph-api/webhooks#setup
 *
 */
function verifyRequestSignature(req, res, buf) {
    var signature = req.headers["x-hub-signature"];

    if (!signature) {
        // For testing, let's log an error. In production, you should throw an
        // error.
        console.error("Couldn't validate the signature.");
    } else {
        var elements = signature.split('=');
        var method = elements[0];
        var signatureHash = elements[1];

        var expectedHash = crypto.createHmac('sha1', FB_APP_SECRET)
            .update(buf)
            .digest('hex');

        if (signatureHash != expectedHash) {
            throw new Error("Couldn't validate the request signature.");
        }
    }
}




app.listen(PORT);
console.log('Listening on :' + PORT + '...');


function bot(sessionId, text, senderId) {
    let context = sessions[sessionId].context

    switch (context.topic) {
        case `serviceMenu`:
            if (!text) {
                // bot says first
                fbMessage(senderId, {
                    text: `gi_serviceMenu`
                });
            } else {
                // user says
                if (text === `buy new policy`) {
                    context.choice = `buyNew`;

                    assignNestedObject(sessions, [sessionId, `context`], context);
                    nextStage(sessionId, senderId);
                } else {

                    // currently other options are not supported
                    fbMessage(senderId, {
                        text: `Please select one of the options`
                    });
                }
            }

            break;

        case `askingConsentToStart`:
            if (!text) {
                fbMessage(senderId, {
                    text: `gi_getStarted`
                });
            } else {
                let consent = giUtility.detectPositive(text);
                if (consent) {
                    context.choice = `yes`;
                    context.person = `self`;
                } else {
                    context.choice = `no`
                }

                assignNestedObject(sessions, [sessionId, `context`], context);
                nextStage(sessionId, senderId);
            }
            break;


        case `askingZipcode`:
            if (!text) {
                fbMessage(senderId, {
                    text: `What is the zipcode your are currently living in?`
                });
            } else {
                Logger.recordConvo(text, `askingZipcode`);
                (async () => {
                    let zipcode;
                    try {
                        zipcode = text.match(/\d+/)[0].toString();
                    } catch (err) {
                        fbMessage(senderId, {
                            text: `I didn't get your zipcode. Would you please repeat?`
                        });
                        return;
                    }
                    try {
                        console.log(`extracted zipcode`, zipcode);
                        context.zipcode = zipcode;
                        context['validateZip'] = await giAPI.validateZip(zipcode);


                        assignNestedObject(sessions, [sessionId, `context`], context);
                        nextStage(sessionId, senderId);

                    } catch (err) {

                        // server error comes here
                        fbMessage(senderId, {
                            text: err.message
                        })
                        fbMessage(senderId, {
                            text: `Would you please say your zipcode again?`
                        });
                    }
                })();
            }
            break;

        case `cfmCounty`:
            if (!text) {
                let county = Object.keys(context.validateZip);
                if (county.length > 1) {
                    // more than one county
                    fbMessage(senderId, {
                        text: `gi_multipleCounty`,
                        data: {
                            county
                        },
                    });
                } else {
                    // only one county
                    let details = context.validateZip[county[0]]
                    if (details.giSupportedState === `false`) {
                        fbMessage(senderId, {
                            text: `gi_callUs`,
                            data: {
                                displayText: `Your county is not supported by GetInsured, please call us.`
                            }
                        });

                        // then we should terminate the conversation, how?
                        delete context.topic;
                        assignNestedObject(sessions, [sessionId, `context`], context);
                        nextStage(sessionId, senderId);
                    } else {
                        context.countyCode = details.countyCode;
                        context.stateCode = details.stateCode;
                        delete context.validateZip;

                        assignNestedObject(sessions, [sessionId, `context`], context);
                        nextStage(sessionId, senderId);
                    }
                }
            } else {
                let county = Object.keys(context.validateZip);
                let match = text.match(new RegExp(county.join("|"), "ig"));
                if (match) {
                    // found
                    let Match = match[0].charAt(0).toUpperCase() + match[0].slice(1);
                    if (context.validateZip[Match].giSupportedState) {
                        context.countyCode = context.validateZip[Match].countyCode;
                        context.stateCode = context.validateZip[Match].stateCode;
                        delete context.validateZip;


                        assignNestedObject(sessions, [sessionId, `context`], context);
                        nextStage(sessionId, senderId);

                    } else {
                        fbMessage(senderId, {
                            text: `gi_callUs`,
                            data: {
                                displayText: `Your county is not supported by GetInsured, please call us.`
                            }
                        });

                        // then we should terminate the conversation, how?
                        delete context.topic;
                        assignNestedObject(sessions, [sessionId, `context`], context);
                        nextStage(sessionId, senderId);
                    }
                } else {
                    // not found
                    fbMessage(senderId, {
                        text: `Sorry I didn't get that. Would you please repeat?`
                    });
                    fbMessage(senderId, {
                        text: `gi_multipleCounty`,
                        data: {
                            county
                        },
                    });
                }
            }
            break;
        case `askingAge`:
            if (!text) {
                switch (context.person) {
                    case `self`:
                        fbMessage(senderId, {
                            text: `What is your age?`
                        });
                        break;
                    case `spouse`:
                        fbMessage(senderId, {
                            text: `What is your spouse's age?`
                        });
                        break;
                    default:
                        fbMessage(senderId, {
                            text: `What is ${context.person}'s age?`
                        });
                }
            } else {
                Logger.recordConvo(text, `askingAge`);
                let age = (() => {
                    let num;
                    try {
                        num = text.match(/\d+$/)[0].toString();
                    } catch (err) {
                        num = nlp.value(text).number.toString();
                    }
                    return num
                })();
                console.log(`extracted age = ${age}`);
                if (age === `0` || !age) {
                    fbMessage(senderId, {
                        text: `Sorry I couldn't get the age. Would you please repeat?`
                    });
                    return;
                } else {
                    // we got the age, storing it to the correct location
                    assignNestedObject(context, [`members`, context[`person`], `age`], age)

                    //if we are currently at children stage, we invoke to loop children 
                    assignNestedObject(sessions, [sessionId, `context`], context);

                    nextStage(sessionId, senderId);
                }

            }
            break;

        case `askingTobacco`:
            if (!text) {
                if (context.members[context.person].age < 18) {
                    assignNestedObject(context, [`members`, context[`person`], `isTobaccoUser`], `N`);
                    assignNestedObject(sessions, [sessionId, `context`], context);
                    loopChildren(sessionId);
                    nextStage(sessionId, senderId);
                } else {

                    fbMessage(senderId, {
                        text: `gi_yesnotobacco`,
                        data: context.person
                    });
                }

            } else {
                Logger.recordConvo(text, `askingTobacco`);
                let tobacco = !!text.includes('yes');
                if (tobacco) {
                    assignNestedObject(context, [`members`, context[`person`], `isTobaccoUser`], `Y`)
                } else {
                    assignNestedObject(context, [`members`, context[`person`], `isTobaccoUser`], `N`)
                }
                assignNestedObject(sessions, [sessionId, `context`], context);

                loopChildren(sessionId);
                nextStage(sessionId, senderId);
            }
            break;

        case `askingIfSpouse`:
            if (!text) {
                // sampath wants 3 buttons here. Use this string as prototype first
                fbMessage(senderId, {
                    text: `gi_askingIfSpouse`
                });
            } else {
                let seekingForSpouse = text.includes('yes');
                if (seekingForSpouse) {
                    context.choice = `yes`;
                    context.person = `spouse`;
                    assignNestedObject(context, [`members`, context[`person`], `isSeekingCoverage`], `Y`);

                    assignNestedObject(sessions, [sessionId, `context`], context);
                    nextStage(sessionId, senderId);
                } else {
                    context.choice = `no`

                    assignNestedObject(sessions, [sessionId, `context`], context);
                    nextStage(sessionId, senderId);

                }

            }


            break;

        case `askingIfChildren`:
            if (!text) {
                fbMessage(senderId, {
                    text: `gi_askingIfChildren`
                });
            } else {
                let seekingForChildren = text.includes('yes');
                if (seekingForChildren) {
                    context.choice = `yes`;
                } else {
                    context.choice = `no`
                }

                assignNestedObject(sessions, [sessionId, `context`], context);
                nextStage(sessionId, senderId);
            }

            break;

        case `askingAmountChildren`:
            if (!text) {
                fbMessage(senderId, {
                    text: `How many children do you have?`
                });
            } else {
                let amountOfChildren = (() => {
                    let num;
                    try {
                        num = text.match(/\d+/)[0];

                    } catch (err) {
                        num = nlp.value(text).number.toString();
                    }
                    return num
                })();
                if (amountOfChildren === `0` || !amountOfChildren) {
                    fbMessage(senderId, {
                        text: `Sorry I couldn't get the number of children. Would you please repeat?`
                    });
                    return;
                } else {
                    // we got the amount, storing it to the correct location
                    context.person = `child1`
                    assignNestedObject(context, [`amountOfChildren`], amountOfChildren);

                    assignNestedObject(sessions, [sessionId, `context`], context);
                    nextStage(sessionId, senderId);
                }
            }

            break;

        case `askingFamilySize`:
            if (!text) {
                fbMessage(senderId, {
                    text: `How many people are there in your family(those who are included in the tax filing)? `
                })
            } else {
                let familySize = (() => {
                    let num;
                    try {
                        num = text.match(/\d+/)[0];

                    } catch (err) {
                        num = nlp.value(text).number.toString();
                    }
                    return num
                })();
                if (familySize === `0` || !familySize) {
                    fbMessage(senderId, {
                        text: `Sorry I couldn't get your family size. Would you please repeat?`
                    });
                    return;
                } else {
                    // we got the amount, storing it to the correct location

                    assignNestedObject(context, [`familySize`], familySize);

                    assignNestedObject(sessions, [sessionId, `context`], context);
                    nextStage(sessionId, senderId);
                }
            }

            break;


        case `askingTotalIncome`:
            if (!text) {
                fbMessage(senderId, {
                    text: `What's your family gross annual income of those who are paying tax? e.g. 20k, 120k`
                });
            } else {
                let income = (() => {
                    let num;
                    try {
                        num = text.match(/(\s|^)(\d+)(,?)(\d+)(k?)(\s|$)/)[0].replace(/,/, "").replace(/k/,"000").trim();
                    } catch (err) {
                        num = nlp.value(text).number;
                    }
                    return num
                })();
                if (income === `0` || !income) {
                    fbMessage(senderId, {
                        text: `Sorry I couldn't get your gross annual income. Would you please repeat?`
                    });
                    return;
                } else {
                    // we got the amount, storing it to the correct location

                    assignNestedObject(context, [`income`], income);
                    (async () => {

                        try {
                            context.plans = await giAPI.screening(context);
                        } catch (err) {
                            context.plans = null;
                            context.fetchError = err;
                        }

                        assignNestedObject(sessions, [sessionId, `context`], context);
                        nextStage(sessionId, senderId);

                    })();
                }
            }
            break;

        case `showPlan`:
            if (!text) {
                fbMessage(senderId, {
                    text: `Here are your plans`
                });
                fbMessage(senderId, {
                    text: `gi_longTemp`,
                    data: context.plans
                });
                fbMessage(senderId, {
                    text: `gi_restartTemp`
                })
            } else {
                // react to user input
                if (text === `restart`) {
                    context = {};

                    assignNestedObject(sessions, [sessionId, `context`], context);
                    nextStage(sessionId, senderId);
                }
            }
            break;

        case `noPlanShowAlternative`:
            if (!text) {
                fbMessage(senderId, {
                    text: `There is a problem fetching the plans.`
                });
                fbMessage(senderId, {
                    text: `Reason: ${context.fetchError}`
                });
                fbMessage(senderId, {
                    text: `gi_restartTemp`
                })
            } else {
                // react to user input
                if (text === `restart`) {
                    context = {};

                    assignNestedObject(sessions, [sessionId, `context`], context);
                    nextStage(sessionId, senderId);
                }
            }

            break;

        case `askQuestion`:
            if (!text) {
                fbMessage(senderId, {
                    text: `gi_askQuestions`
                });
            } else {
                if (text.includes(`yes`)) {
                    nextStage(sessionId, senderId);
                } else if (text.includes(`no`)) {
                    fbMessage(senderId, {
                        text: `gi_askQuestions`
                    });
                }
                fbMessage(senderId, {
                    text: `the answer is blah blah blah`
                });
                fbMessage(senderId, {
                    text: `gi_returnToFlow`
                });

            }
            break;


        default:
            (async () => {
                let firstname
                try {
                    firstname = await fbAPI.getFirstName(senderId);
                    fbMessage(senderId, {
                        text: `Hi ${firstname}. I'm Gi-gene, your friendly virtual agent.`
                    });
                } catch (err) {
                    console.log(`fail to get first name`, err);
                    fbMessage(senderId, {
                        text: `Hi. I'm Gi-gene, your friendly virtual agent.`
                    });
                }
                nextStage(sessionId, senderId)

            })();
    }

    // do not to put anything here. There are asyncronous function call inside. after the case break, the execution will come here. 
}

// this method figures out what "topic"" the bot will talk about next
function nextStage(sessionId, senderId) {
    let context = sessions[sessionId].context;
    console.log('\x1b[33m', "context:", context, '\x1b[0m');


    //  ===============  starter =========================== 
    switch (context.topic) {
        case `serviceMenu`:
            switch (context.choice) {
                case `buyNew`:
                    context.topic = `askingConsentToStart`;

                    break;

                // should add in support for other options soon
            }
            break;

        case `askingConsentToStart`:
            switch (context.choice) {
                case `yes`:
                    context.topic = `askingZipcode`;
                    break;
                case `no`:
                    context.topic = `serviceMenu`;
                    break;
            }

            break;

        case `askingZipcode`:
            context.topic = `cfmCounty`;
            break;

        case `cfmCounty`:
            context.topic = `askingAge`;
            break;

        case `askingAge`:
            context.topic = `askingTobacco`;
            break;

        case `askingTobacco`:

            switch (context.person) {
                case `self`:
                    // case for first time reach => asking spouse
                    context.topic = `askingIfSpouse`;
                    break;
                case `spouse`:
                    // case for finish asking spouse => asking children
                    context.topic = `askingIfChildren`;
                    break;
                case `none`:
                    // case for we are done with all people
                    context.topic = `askingFamilySize`;
                    break;
                default:
                    //case whereby we are looping for children => jump back to ask age of the next child
                    context.topic = `askingAge`;
            }


            break;

        case `askingIfSpouse`:
            if (context.choice === `yes`) {
                // if they are seeking to cover spouse
                context.topic = `askingAge`;
            } else {
                // if they are not seeking to cover spouse
                context.topic = `askingIfChildren`;
            }


            break;

        case `askingIfChildren`:
            if (context.choice === `yes`) {
                context.topic = `askingAmountChildren`;
            } else {
                context.topic = `askingFamilySize`;
            }

            break;

        case `askingAmountChildren`:
            context.topic = `askingAge`;

            break;

        case `askingFamilySize`:
            context.topic = `askingTotalIncome`;

            break;


        case `askingTotalIncome`:
            if (context.plans) {
                context.topic = `showPlan`
            } else {
                context.topic = `noPlanShowAlternative`
            }

            break;

        case `askQuestion`:
            context.topic = context.cachTopic;
            delete context.cachTopic;

        default:
            context[`topic`] = `serviceMenu`;
    }

    // clean up temp memory
    delete context.choice;
    // update context
    assignNestedObject(sessions, [sessionId, `context`], context);
    bot(sessionId, null, senderId);
    return;
}

const assignNestedObject = function (base, names, value) {
    // If a value is given, remove the last name and keep it for later:
    var lastName = arguments.length === 3 ? names.pop() : false;

    // Walk the hierarchy, creating new objects where needed.
    // If the lastName was removed, then the last object is not set yet:
    for (var i = 0; i < names.length; i++) {
        base = base[names[i]] = base[names[i]] || {};
    }

    // If a value was given, set it to the last name:
    if (lastName) base = base[lastName] = value;

    // Return the last object in the hierarchy:
    return base;
};

const loopChildren = (sessionId) => {
    let context = sessions[sessionId].context;
    if (context.person.includes(`child`)) {
        let trackingNum = parseInt(context.person.charAt(5));
        if (trackingNum === parseInt(context.amountOfChildren)) {
            context.person = `none`;
        } else {
            context.person = `child${trackingNum + 1}`;
        }
    }

    assignNestedObject(sessions, [sessionId, `context`], context);
}

// fbMessage("1215326618535083",{text:`hello`});


// let plans = [
//     {
//         "id": 8,
//         "issuerPlanNumber": "40513CA038000301",
//         "name": "Silver 70 HMO",
//         "level": "SILVER",
//         "premium": 382.38,
//         "issuer": "Kaiser Foundation Health Plan of California",
//         "issuerText": "kaiserfoundationhealthplanofcalifornia",
//         "networkType": "HMO",
//         "networkKey": "40513-CAN001-2016",
//         "planScore": 0,
//         "rawOutOfPocketCost": 0,
//         "outOfPocketEstimate": 0,
//         "premiumBeforeCredit": 382.38,
//         "annualPremiumBeforeCredit": 4588.56,
//         "aptc": 100,
//         "totalContribution": 0,
//         "premiumAfterCredit": 282.38,
//         "annualPremiumAfterCredit": 3388.56,
//         "costSharingReductions": 0,
//         "adjustedOop": 0,
//         "oopMax": null,
//         "childOopMax": null,
//         "maxTotalHealthCareCost": 0,
//         "estimatedTotalHealthCareCost": 3388.56,
//         "costSharing": "CS1",
//         "deductible": null,
//         "intgMediDrugDeductible": null,
//         "medicalDeductible": null,
//         "drugDeductible": null,
//         "planId": 407018,
//         "groupId": null,
//         "groupName": null,
//         "groupMembers": null,
//         "orderItemId": null,
//         "result": null,
//         "planType": "HEALTH",
//         "ehb": 0,
//         "enrollmentId": null,
//         "oldPremium": null,
//         "smartScore": null,
//         "expenseEstimate": null,
//         "qoutingGroup": null,
//         "ehbPrecentage": null,
//         "ehbPremium": null,
//         "qualityRating": null,
//         "planTier1": null,
//         "planTier2": null,
//         "planOutNet": null,
//         "planAppliesToDeduct": null,
//         "netwkException": null,
//         "missingDentalCovg": null,
//         "benefitExplanation": null,
//         "issuerQualityRating": null,
//         "planDetailsByMember": [
//             {
//                 "premium": "382.380005",
//                 "id": "1",
//                 "region": "8"
//             }
//         ],
//         "doctors": [],
//         "facilities": null,
//         "dentists": null,
//         "doctorsCount": 0,
//         "facilitiesCount": 0,
//         "dentistsCount": 0,
//         "doctorsSupported": "not-supported",
//         "facilitiesSupported": null,
//         "dentistsSupported": null,
//         "totalProvidersCount": 0,
//         "docfacilityCount": 0,
//         "overAllQuality": null,
//         "oldAptc": null,
//         "sbcUrl": "",
//         "planBrochureUrl": "",
//         "planCosts": null,
//         "providerLink": "https://www.kaiserpermanente.org",
//         "issuerLogo": "https://9b5ea118f3d68744fa04-04b308c70d701305ff31abd2a6fa528a.ssl.cf1.rackcdn.com/resources/issuers/logo//2015-10-1415:26:274/4148_40513Kaiser_1435254920613.png",
//         "issuerId": 4148,
//         "maxCoinseForSpecialtyDrugs": null,
//         "maxNumDaysForChargingInpatientCopay": null,
//         "primaryCareCostSharingAfterSetNumberVisits": "",
//         "primaryCareDeductOrCoinsAfterSetNumberCopays": "",
//         "benefitsCoverage": null,
//         "isPuf": "N",
//         "formularyUrl": null,
//         "hsa": "No",
//         "providers": null,
//         "coinsurance": null,
//         "policyLength": null,
//         "policyLimit": null,
//         "applicationFee": null,
//         "intgMediDrugOopMax": null,
//         "medicalOopMax": null,
//         "drugOopMax": null,
//         "dentalGuarantee": null,
//         "exchangeType": "ON",
//         "separateDrugDeductible": null,
//         "outOfNetwkCoverage": null,
//         "outOfCountyCoverage": null,
//         "policyLengthUnit": null,
//         "oopMaxFamily": null,
//         "oopMaxIndDesc": null,
//         "optionalDeductible": null,
//         "contributionData": null,
//         "maxBenefits": null,
//         "providerDensity": null,
//         "copayGenericDrug": null,
//         "copayOfficeVisit": null,
//         "annualLimit": null,
//         "tier2Coverage": null,
//         "encryptedPremiumBeforeCredit": null,
//         "encryptedPremiumAfterCredit": null,
//         "dentalPlanId": null,
//         "amePlanId": null,
//         "dentalPlanPremium": null,
//         "amePlanPremium": null,
//         "encryptedDentalPlanPremium": null,
//         "encryptedAmePlanPremium": null,
//         "encodedPremium": "Pi2LZrpIddcl30UM9Ys0lQ==",
//         "visionEyeExam": 0,
//         "visionGlasses": 0,
//         "visionContacts": 0,
//         "visionPlanId": null,
//         "visionPlanPremium": null,
//         "recommendationRank": 2,
//         "coverageAmount": null,
//         "policyTerm": null,
//         "benefitUrl": null,
//         "networkTransparencyRating": null,
//         "prescriptionResponseList": [],
//         "planTab": "TAX_CREDIT"
//     },
//     {
//         "id": 10,
//         "issuerPlanNumber": "40513CA038000501",
//         "name": "Bronze 60 HSA HMO",
//         "level": "BRONZE",
//         "premium": 283.69,
//         "issuer": "Kaiser Foundation Health Plan of California",
//         "issuerText": "kaiserfoundationhealthplanofcalifornia",
//         "networkType": "HMO",
//         "networkKey": "40513-CAN001-2016",
//         "planScore": 0,
//         "rawOutOfPocketCost": 0,
//         "outOfPocketEstimate": 0,
//         "premiumBeforeCredit": 283.69,
//         "annualPremiumBeforeCredit": 3404.28,
//         "aptc": 100,
//         "totalContribution": 0,
//         "premiumAfterCredit": 183.69,
//         "annualPremiumAfterCredit": 2204.28,
//         "costSharingReductions": 0,
//         "adjustedOop": 0,
//         "oopMax": null,
//         "childOopMax": null,
//         "maxTotalHealthCareCost": 0,
//         "estimatedTotalHealthCareCost": 2204.28,
//         "costSharing": "CS1",
//         "deductible": null,
//         "intgMediDrugDeductible": null,
//         "medicalDeductible": null,
//         "drugDeductible": null,
//         "planId": 407027,
//         "groupId": null,
//         "groupName": null,
//         "groupMembers": null,
//         "orderItemId": null,
//         "result": null,
//         "planType": "HEALTH",
//         "ehb": 0,
//         "enrollmentId": null,
//         "oldPremium": null,
//         "smartScore": null,
//         "expenseEstimate": null,
//         "qoutingGroup": null,
//         "ehbPrecentage": null,
//         "ehbPremium": null,
//         "qualityRating": null,
//         "planTier1": null,
//         "planTier2": null,
//         "planOutNet": null,
//         "planAppliesToDeduct": null,
//         "netwkException": null,
//         "missingDentalCovg": null,
//         "benefitExplanation": null,
//         "issuerQualityRating": null,
//         "planDetailsByMember": [
//             {
//                 "premium": "283.690002",
//                 "id": "1",
//                 "region": "8"
//             }
//         ],
//         "doctors": [],
//         "facilities": null,
//         "dentists": null,
//         "doctorsCount": 0,
//         "facilitiesCount": 0,
//         "dentistsCount": 0,
//         "doctorsSupported": "not-supported",
//         "facilitiesSupported": null,
//         "dentistsSupported": null,
//         "totalProvidersCount": 0,
//         "docfacilityCount": 0,
//         "overAllQuality": null,
//         "oldAptc": null,
//         "sbcUrl": "",
//         "planBrochureUrl": "",
//         "planCosts": null,
//         "providerLink": "https://www.kaiserpermanente.org",
//         "issuerLogo": "https://9b5ea118f3d68744fa04-04b308c70d701305ff31abd2a6fa528a.ssl.cf1.rackcdn.com/resources/issuers/logo//2015-10-1415:26:274/4148_40513Kaiser_1435254920613.png",
//         "issuerId": 4148,
//         "maxCoinseForSpecialtyDrugs": null,
//         "maxNumDaysForChargingInpatientCopay": null,
//         "primaryCareCostSharingAfterSetNumberVisits": "",
//         "primaryCareDeductOrCoinsAfterSetNumberCopays": "",
//         "benefitsCoverage": null,
//         "isPuf": "N",
//         "formularyUrl": null,
//         "hsa": "Yes",
//         "providers": null,
//         "coinsurance": null,
//         "policyLength": null,
//         "policyLimit": null,
//         "applicationFee": null,
//         "intgMediDrugOopMax": null,
//         "medicalOopMax": null,
//         "drugOopMax": null,
//         "dentalGuarantee": null,
//         "exchangeType": "ON",
//         "separateDrugDeductible": null,
//         "outOfNetwkCoverage": null,
//         "outOfCountyCoverage": null,
//         "policyLengthUnit": null,
//         "oopMaxFamily": null,
//         "oopMaxIndDesc": null,
//         "optionalDeductible": null,
//         "contributionData": null,
//         "maxBenefits": null,
//         "providerDensity": null,
//         "copayGenericDrug": null,
//         "copayOfficeVisit": null,
//         "annualLimit": null,
//         "tier2Coverage": null,
//         "encryptedPremiumBeforeCredit": null,
//         "encryptedPremiumAfterCredit": null,
//         "dentalPlanId": null,
//         "amePlanId": null,
//         "dentalPlanPremium": null,
//         "amePlanPremium": null,
//         "encryptedDentalPlanPremium": null,
//         "encryptedAmePlanPremium": null,
//         "encodedPremium": "Ip8jH911lI7OKuD6ZLrJlw==",
//         "visionEyeExam": 0,
//         "visionGlasses": 0,
//         "visionContacts": 0,
//         "visionPlanId": null,
//         "visionPlanPremium": null,
//         "recommendationRank": 1,
//         "coverageAmount": null,
//         "policyTerm": null,
//         "benefitUrl": null,
//         "networkTransparencyRating": null,
//         "prescriptionResponseList": [],
//         "planTab": "TAX_CREDIT"
//     }
// ]

// fbMessage("1215326618535083", {
//     text: `gi_longTemp`,
//     data: plans
// });